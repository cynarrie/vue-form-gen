var data = [{
        "type": "text",
        "label": "First Name",
        "model": "name",
        "mute": "this is message",
        "placeholder": "place holder",
        "value": "Andy"
    },
    {
        "type": "password",
        "label": "Last Name",
        "model": "lname",
        "mute": "this is message",
        "placeholder": "place holder",
        "value": "wong"
    },
    {
        "type": "checkbox",
        "label": "Remeber",
        "model": "rem",
        "mute": "",
        "placeholder": "",
        "value": true
    },
    {
        "type": "number",
        "label": "how many",
        "model": "num",
        "mute": "",
        "placeholder": "",
        "value": 3
    },
    {
        "type": "date",
        "label": "When",
        "model": "ddate",
        "mute": "",
        "placeholder": "",
        "value": ""
    },
    {
        "type": "select",
        "label": "Sex",
        "model": "sex",
        "mute": "this is select",
        "placeholder": ["Male", "Female"],
        "value": ""
    },
    {
        "type": "radio",
        "label": "Happy?",
        "model": "happy",
        "mute": "this is radio",
        "placeholder": ["Yes", "No"],
        "value": ""
    },
    {
        "type": "image",
        "label": "Image",
        "model": "image",
        "mute": "this is image",
        "placeholder": "",
        "value": ""
    }
].reverse()

var J8 = {
    form: {
        vuecomp: {},
        vue: function(el, obj) {
            var formstr = ""
            var modeldata = {}
            for (var i = obj.length - 1; i >= 0; i--) {
                formstr += this.bootstrap[obj[i].type](obj[i].label, obj[i].model, obj[i].mute, obj[i].placeholder)
                modeldata[obj[i].model] = obj[i].value;
            }
            document.getElementById(el).innerHTML = formstr;
            return this.vuecomp[el] = new Vue({
                el: '#' + el,
                data: modeldata,
                methods: {
                    "uploadimage": function(ev) {
                        var model = ev.target.id;
                        var fileReader = new FileReader();
                        fileReader.onload = function() {
                            imgdata = fileReader.result;
                            J8.form.vuecomp[el][model] = imgdata;
                        };
                        fileReader.readAsDataURL(ev.target.files[0]);
                    }
                }

            })
        },
        bootstrap: {
            text: function(label, model, mute, placeholder) {
                var str = '<div class="form-group">' +
                    '<label>' + label + '</label>' +
                    '<input type="text" class="form-control" v-model="' + model + '" placeholder="' + placeholder + '">' +
                    '<small class="form-text text-muted">' + mute + '</small>' +
                    '</div>'
                return str;
            },
            email: function(label, model, mute, placeholder) {
                var str = '<div class="form-group">' +
                    '<label>' + label + '</label>' +
                    '<input type="email" class="form-control" v-model="' + model + '" placeholder="' + placeholder + '">' +
                    '<small class="form-text text-muted">' + mute + '</small>' +
                    '</div>'
                return str;

            },
            number: function(label, model, mute, placeholder) {
                var str = '<div class="form-group">' +
                    '<label>' + label + '</label>' +
                    '<input type="number" class="form-control" v-model="' + model + '" placeholder="' + placeholder + '">' +
                    '<small class="form-text text-muted">' + mute + '</small>' +
                    '</div>'
                return str;

            },
            date: function(label, model, mute, placeholder) {
                var str = '<div class="form-group">' +
                    '<label>' + label + '</label>' +
                    '<input type="date" class="form-control" v-model="' + model + '" placeholder="' + placeholder + '">' +
                    '<small class="form-text text-muted">' + mute + '</small>' +
                    '</div>'
                return str;

            },
            select: function(label, model, mute, placeholder) {
                var str = '<div class="form-group">' +
                    '<label>' + label + '</label>' +
                    '<select class="form-control" v-model="' + model + '">'
                for (var i = placeholder.length - 1; i >= 0; i--) {
                    str += '<option>' + placeholder[i] + '</option>'
                }

                str += '</select>' +
                    '<small class="form-text text-muted">' + mute + '</small>' +
                    '</div>'
                return str;
            },
            checkbox: function(label, model, mute, placeholder) {
                var str = '<div class="form-check">' +
                    '<input type="checkbox" v-model="' + model + '" class="form-check-input">&nbsp;' +
                    '<label class="form-check-label">' + label + '</label>' +
                    '<small class="form-text text-muted">' + mute + '</small>' +
                    '</div>'
                return str;

            },
            radio: function(label, model, mute, placeholder) {
                var str = ''
                for (var i = placeholder.length - 1; i >= 0; i--) {
                    str += '<div class="form-check">'
                    str += '<input class="form-check-input" type="radio" name="model" v-model="' + model + '" value="' + placeholder[i] + '">' +
                        '<label class="form-check-label">&nbsp;' + placeholder[i] + '</label></div>'
                }
                str += '<small class="form-text text-muted">' + mute + '</small>';
                return str;
            },
            textarea: function(label, model, mute, placeholder) {
                var str = '<div class="form-group">' +
                    '<label>' + label + '</label>' +
                    '<textarea class="form-control" v-model="' + model + '" rows="3"></textarea>' +
                    '<small class="form-text text-muted">' + mute + '</small>' +
                    '</div>'
                return str;

            },
            image: function(label, model, mute, placeholder) {
                var str = '<div class="form-group">' +
                    '<label>' + label + '</label>' +
                    '<input type="file" class="form-control-file" id="' + model + '" v-on:change="uploadimage">' +
                    '<img :src="image"  class="img-fluid">' +
                    '</div>'
                return str;
            },
            password: function(label, model, mute, placeholder) {
                var str = '<div class="form-group">' +
                    '<label>' + label + '</label>' +
                    '<input type="password" class="form-control" v-model="' + model + '" placeholder="' + placeholder + '">' +
                    '<small class="form-text text-muted">' + mute + '</small>' +
                    '</div>'
                return str;
            }
        }
    }
}
